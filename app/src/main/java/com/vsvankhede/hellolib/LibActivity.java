package com.vsvankhede.hellolib;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LibActivity extends AppCompatActivity {

    // Fix 1.0.1
    // 5 feature added
    public static void start(Context context) {
        Intent starter = new Intent(context, LibActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lib);
    }
}
